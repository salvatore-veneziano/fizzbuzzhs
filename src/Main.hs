module Main where

import Data.Foldable (traverse_)
import Data.Maybe (fromMaybe)

when :: String -> Integer -> Integer -> Maybe String
when string value x
  | x `mod` value == 0 = Just string
  | otherwise = Nothing

fizz, buzz, bang :: Integer -> Maybe String
fizz = "Fizz" `when` 3
buzz = "Buzz" `when` 5
bang = "Bang" `when` 7

(?:) :: (t -> Maybe a) -> (t -> a) -> t -> a
(?:) f g x = fromMaybe (g x) (f x)

infix 5 ?:

fizzBuzz :: Integer -> String
fizzBuzz = fizz <> buzz <> bang ?: show

main :: IO ()
main = traverse_ (print . fizzBuzz) [1 .. 200]
